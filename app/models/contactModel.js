// Bước 1: Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Khai báo thư viện unique validator
const uniqueValidator = require('mongoose-unique-validator');

// Bước 2: Khai báo class schema
const Schema = mongoose.Schema;

// Bước 3: Khởi tạo schema với các thuộc tính của collection
const contactSchema = new Schema({
  _id: mongoose.Types.ObjectId,
  email: {
    type: String,
    lowercase: true,
    required: [true, "can't be blank"],
    unique: true,
    match: [/\S+@\S+\.\S+/, 'is invalid'],
    index: true
  }
}, { timestamps: true });

// Sử dụng unique validator
contactSchema.plugin(uniqueValidator, { message: 'is already taken.' });

// Bước 4: Biên dịch schema thành model
module.exports = mongoose.model('contact', contactSchema);
