//import thư viện mongoose
const mongoose = require('mongoose');

// import Contact model
const contactModel = require('../models/contactModel');

//get all contacts
const getAllContact = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const contactList = await contactModel.find();

        if (contactList && contactList.length > 0) {
            return res.status(200).json({
                status: "Get all Contact sucessfully",
                data: contactList
            })
        } else {
            return res.status(404).json({
                status: "Not found any Contact"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

//get a contact
const getContactById = async (req, res) => {
    //B1: thu thập dữ liệu
    var contactId = req.params.contactId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const contactInfo = await contactModel.findById(contactId);

        if (contactInfo) {
            return res.status(200).json({
                status: "Get Contact by id sucessfully",
                data: contactInfo
            })
        } else {
            return res.status(404).json({
                status: "Not found any Contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//create Contact 
const createContact = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        email
    } = req.body;

    // B2: Validate dữ liệu
    if (!email) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "email is require"
        })
    }

    // B3: Thao tác với CSDL
    var newContact = {
        _id: new mongoose.Types.ObjectId(),
        email,
    }

    try {
        const createdContact = await contactModel.create(newContact);
        return res.status(201).json({
            status: "Create Contact successfully",
            data: createdContact
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

//update contact
const updateContact = async (req, res) => {
    //B1: thu thập dữ liệu
    var contactId = req.params.contactId;

    const {
        email,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!email) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "email is require"
        })
    }

    //B3: thực thi model
    try {
        let updateContact = {
            email,
        }

        const updatedContact = await contactModel.findByIdAndUpdate(contactId, updateContact);

        if (updatedContact) {
            return res.status(200).json({
                status: "Update Contact sucessfully",
                data: updatedContact
            })
        } else {
            return res.status(404).json({
                status: "Not found any Contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete contact
const deleteContact = async (req, res) => {
    //B1: Thu thập dữ liệu
    var contactId = req.params.contactId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        const deletedContact = await contactModel.findByIdAndDelete(contactId);

        if (deletedContact) {
            return res.status(200).json({
                status: "Delete Contact sucessfully",
                data: deletedContact
            })
        } else {
            return res.status(404).json({
                status: "Not found any Contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    getAllContact,
    createContact,
    getContactById,
    updateContact,
    deleteContact
}

