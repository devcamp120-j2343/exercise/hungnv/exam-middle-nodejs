//Import thu vien express
const express = require("express");
//Khai bao middleware
const userMiddleware = require("../middlewares/userMiddleware");
//import controller
const { getAllUser, createUser, getUserById, updateUser, deleteUser } = require("../controllers/userController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL User: ", req.url);

    next();
});
//get all customer
router.get("/", userMiddleware.getAllUserMiddleware, getAllUser)
//create customer
router.post("/", userMiddleware.postUserMiddleware, createUser);
//get a customer
router.get("/:userId", userMiddleware.getAUserMiddleware, getUserById);
//update customer
router.put("/:userId", userMiddleware.putUserMiddleware, updateUser);
//delete customer
router.delete("/:userId", userMiddleware.deleteUserMiddleware, deleteUser);

module.exports = router;