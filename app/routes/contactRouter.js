//Import thu vien express
const express = require("express");
//Khai bao middleware
const contactMiddleware = require("../middlewares/contactMiddleware");
//import controller
const { getAllContact, createContact, getContactById, updateContact, deleteContact } = require("../controllers/contactController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL Contact: ", req.url);

    next();
});
//get all customer
router.get("/", contactMiddleware.getAllContacttMiddleware, getAllContact)
//create customer
router.post("/", contactMiddleware.postContacttMiddleware, createContact);
//get a customer
router.get("/:contactId", contactMiddleware.getAContacttMiddleware, getContactById);
//update customer
router.put("/:contactId", contactMiddleware.putContacttMiddleware, updateContact);
//delete customer
router.delete("/:contactId", contactMiddleware.deleteContacttMiddleware, deleteContact);

module.exports = router;