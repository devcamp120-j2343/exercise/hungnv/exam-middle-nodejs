const getAllContacttMiddleware = (req, res, next) => {
    console.log("Get All Contactt!");
    next();
}

const getAContacttMiddleware = (req, res, next) => {
    console.log("Get a Contactt!");
    next();
}
const postContacttMiddleware = (req, res, next) => {
    console.log("Create a Contactt!");
    next();
}

const putContacttMiddleware = (req, res, next) => {
    console.log("Update a Contactt!");
    next();
}
const deleteContacttMiddleware = (req, res, next) => {
    console.log("Delete a Contactt!");
    next();
}

//export
module.exports = {
    getAllContacttMiddleware,
    getAContacttMiddleware,
    postContacttMiddleware,
    putContacttMiddleware,
    deleteContacttMiddleware
}