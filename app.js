const express = require("express"); // Tương tự : import express from "express";
const path = require("path");
//Import thu vien mongoose
const mongoose = require('mongoose');
//Khai bao model mongoose
//khai báo các model
const userModel = require('./app/models/userModel');
const contactModel = require('./app/models/contactModel');
//khai bao router
const userRouter = require("./app/routes/userRouter");
const contactRouter = require("./app/routes/contactRouter");
// Khởi tạo app nodejs bằng express
const app = express();
// Kết nối với MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Vaccination")
  .then(() => {
    console.log('Successfully connected to MongooseDB');
  })
  .catch((error) => {
    throw error;
  });
// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());

//Khai bao ung dung se chay cong 8000
const port = 8000;

//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/app/views/page"));

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
  console.log(__dirname);

  response.sendFile(path.join(__dirname + "/app/views/page/index.html"));
})
app.get("/admin/user", (request, response) => {
  console.log(__dirname);
  response.sendFile(path.join(__dirname + "/app/views/admin/user/userManagement.html"));
})
app.get("/admin/contact", (request, response) => {
  console.log(__dirname);
  response.sendFile(path.join(__dirname + "/app/views/admin/contact/contactManagement.html"));
})

app.use((request, response, next) => {
  var today = new Date();

  console.log("Hom nay la: ", today);

  next();
});


app.use((request, response, next) => {
  console.log("Method: ", request.method);

  next();
});


app.use("/api/v1/user", userRouter);
app.use("/api/v1/contact", contactRouter);

app.listen(port, () => {
  console.log(`App Listening on port ${port}`);
})